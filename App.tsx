/* eslint-disable @typescript-eslint/no-unused-vars */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import {firebase} from '@react-native-firebase/auth';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import React, {useEffect} from 'react';
import {StyleSheet, Text} from 'react-native';
import {Button, Checkbox} from 'react-native-paper';
import {SafeAreaView} from 'react-native-safe-area-context';
import AuthGuard from './src/auth/AuthGuard';
import LoginPage from './src/screens/Login';
const firebaseConfig = {
  apiKey: 'AIzaSyDNTyH8XwX-CkMGTz8YtrQyFycxWQIbWWg',
  authDomain: 'aware-me-app.firebaseapp.com',
  projectId: 'aware-me-app',
  storageBucket: 'aware-me-app.appspot.com',
  messagingSenderId: '434666689536',
  appId: '1:434666689536:web:fa07af8c8809d88e9534e5',
  measurementId: 'G-0NDHNGLS1F',
};
const App = () => {
  // useEffect(() => {
  //   return () => {
  //     firebase.initializeApp(firebaseConfig);
  //   };
  // }, []);
  return (
    <>
      {/* <SafeAreaView> */}
      <AuthGuard />
      {/* </SafeAreaView> */}
    </>
  );
};

const styles = StyleSheet.create({});

export default App;
