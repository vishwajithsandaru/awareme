import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {Text, View} from 'react-native';
import {Button} from 'react-native-paper';
export interface HomeProps {}

const Home: React.FC<HomeProps> = () => {
  const nav = useNavigation();
  return (
    <View>
      <Text>You are Safe</Text>
      <Button mode="text" onPress={() => nav.navigate('Login')}>
        LogOut
      </Button>
    </View>
  );
};

export default Home;
