import {GoogleSignin} from '@react-native-community/google-signin';
import React, {useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Button, Headline} from 'react-native-paper';
import auth from '@react-native-firebase/auth';
import {useNavigation} from '@react-navigation/native';
export interface LoginPageProps {}

const LoginPage: React.FC<LoginPageProps> = () => {
  const nav = useNavigation();
  useEffect(() => {
    GoogleSignin.configure({
      webClientId:
        '434666689536-iqp3hhbvs7vu5tbqacbuakmg76quu2hm.apps.googleusercontent.com',
    });
  }, []);
  const handleGoogleLogin = async () => {
    // const {idToken} = await GoogleSignin.signIn();
    // const cred = auth.GoogleAuthProvider.credential(idToken);
    // auth().signInWithCredential(cred);
    nav.navigate('Home');
  };
  return (
    <View style={styles.rootContainer}>
      <View style={styles.logoContainer}>
        <Headline style={styles.logo}>Aware Me!</Headline>
      </View>
      <Button
        style={styles.button}
        onPress={handleGoogleLogin}
        mode="contained"
        icon="google">
        Login with Google
      </Button>
    </View>
  );
};

const styles = StyleSheet.create({
  rootContainer: {
    alignContent: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
  },
  logoContainer: {},
  logo: {
    textAlign: 'center',
    padding: 20,
    fontSize: 50,
    color: '#4285F4',
  },

  button: {
    width: '50%',
  },
});
export default LoginPage;
