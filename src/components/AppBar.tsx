import {
  useNavigation,
  useNavigationState,
  useRoute,
} from '@react-navigation/native';
import {Scene} from '@react-navigation/stack/lib/typescript/src/types';
import * as React from 'react';
import {Appbar} from 'react-native-paper';

export interface AppBarProps {
  previous?: Scene<any>;
}

const AppBar: React.FC<AppBarProps> = ({previous}) => {
  const route = useRoute();
  const nav = useNavigation();
  return (
    <Appbar.Header>
      {previous ? <Appbar.BackAction onPress={() => nav.goBack()} /> : null}
      <Appbar.Content title={route.name} />
      <Appbar.Action
        icon="cog-outline"
        onPress={() => nav.navigate('Settings')}
      />
    </Appbar.Header>
  );
};

export default AppBar;
