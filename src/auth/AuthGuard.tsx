/* eslint-disable @typescript-eslint/no-unused-vars */
import React, {useCallback, useEffect, useState} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import LoginPage from '../screens/Login';
import Home from '../screens/Home';
import {RouteList} from '../routes';
import AppBar from '../components/AppBar';
import auth, {FirebaseAuthTypes} from '@react-native-firebase/auth';
export interface AuthGuardProps {}
const Stack = createStackNavigator();

export interface RouteObject {
  name: string;
  compo: React.FC<any>;
  isGuarded?: boolean;
}

const AuthGuard: React.FC<AuthGuardProps> = () => {
  const [user, setUser] = useState<FirebaseAuthTypes.User | null>();
  const onAuthChange = useCallback(
    (user: FirebaseAuthTypes.User | null) => {
      setUser(user);
    },
    [setUser],
  );

  useEffect(() => {
    return () => {
      auth().onAuthStateChanged(onAuthChange);
    };
  }, []);
  return (
    <>
      <Stack.Navigator
        screenOptions={{header: (props) => <AppBar {...props} />}}
        initialRouteName={user ? 'Home' : 'Login'}>
        {RouteList.map((item, i) => {
          return (
            <Stack.Screen key={i} name={item.name} component={item.compo} options={{headerShown: (item.name == "Login")?false:true}} />
          );
        })}
      </Stack.Navigator>
    </>
  );
};

export default AuthGuard;
