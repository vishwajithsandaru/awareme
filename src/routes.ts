import {RouteObject} from './auth/AuthGuard';
import Home from './screens/Home';
import LoginPage from './screens/Login';
export const RouteList: RouteObject[] = [
  {compo: LoginPage, name: 'Login', isGuarded: false},
  {compo: Home, name: 'Home'},
];
